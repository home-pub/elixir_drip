use Mix.Config

import_config "#{Mix.env}.exs"


config :elixir_drip, 
	ecto_repos: [ElixirDrip.Repo],
	storage_provider: ElixirDrip.Storage.Providers.GoogleCloudStorageLocal
