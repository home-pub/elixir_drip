use Mix.Config

# Configure your database
config :elixir_drip, ElixirDrip.Repo,
  adapter: Ecto.Adapters.MySQL,
  username: "root",
  password: "",
  database: "elixir_drip_dev",
  hostname: "localhost",
  pool_size: 10
